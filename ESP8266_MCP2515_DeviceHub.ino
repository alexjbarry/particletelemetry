// CAN Receive Example
//

#include <mcp_can.h>
#include <SPI.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Define outputs as per labels on NodeMCU 
#define D0 16
#define D1 5 // I2C Bus SCL (clock)
#define D2 4 // I2C Bus SDA (data)
#define D3 0
#define D4 2 // Same as "LED_BUILTIN", but inverted logic
#define D5 14 // SPI Bus SCK (clock)
#define D6 12 // SPI Bus MISO 
#define D7 13 // SPI Bus MOSI
#define D8 15 // SPI Bus SS (CS)
#define D9 3 // RX0 (Serial console)
#define D10 1 // TX0 (Serial console)

// CAN0 INT and CS
#define CAN0_CS D2
#define CAN0_INT D1
MCP_CAN CAN0(CAN0_CS);                               // Set CS to pin 4 (D2 on my NodeMCU)

//Definitions for DeviceHub.net
#define API_KEY         "27017f31-861f-4434-a375-b6f884a82851"
#define PROJECT_ID      "8265"
#define STRING_NAME     "stringmsg"
#define SENSOR_RPM      "j1939_engine_speed"
#define SENSOR_TEMP     "j1939_engine_temp"
#define DEVICE_UUID     "2a3783b7-35ab-49f6-89a4-ff022ae5e37d"

//Definitions for WiFi
#define SSID  "JANOOB"
#define PASSWORD "yellowjeep"

WiFiClient espClient;
PubSubClient client(espClient);

//init vars for CAN
long unsigned int rxId;
unsigned char len = 0;
unsigned char rxBuf[8];
char msgString[128];                        // Array to store serial string
byte data[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

//init vars for DeviceHub.net
char stringTopic[]    = "/a/"API_KEY"/p/"PROJECT_ID"/d/"DEVICE_UUID"/sensor/"STRING_NAME"/data";
char mqtt_server[]    = "mqtt.devicehub.net"; // server mqtt.devicehub.net

//Connect to local WiFi
void setup_wifi()
{ 
  Serial.print("Connecting to ");
  Serial.println(SSID);
  
  WiFi.begin(SSID, PASSWORD);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP()); 
}

//Configure MCP2515
void setup_mcp2515()
{
  // Initialize MCP2515 running at 8MHz with a baudrate of 500kb/s and the masks and filters disabled.
  if(CAN0.begin(MCP_ANY, CAN_250KBPS, MCP_8MHZ) == CAN_OK)
    Serial.println("MCP2515 Initialized Successfully!");
  else
    Serial.println("Error Initializing MCP2515...");
  
  CAN0.setMode(MCP_NORMAL);   // Set operation mode to normal so the MCP2515 sends acks to received data.

  pinMode(CAN0_INT, INPUT);   // Configuring pin for /INT input
}

// Handle arriving MQTT messages
void callback(char* topic, byte* payload, unsigned int length)
{
  // do nothing for now
}

//MQTT connection to DeviceHub.net
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(millis()), HEX);
    // Attempt to connectn
    if (client.connect(clientId.c_str())) {
      Serial.println(" connected");
      client.subscribe("inTopic");
    } else {
      Serial.print(" failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup(void)  
{
  Serial.begin(115200);   // init serial for debugging
  
  setup_wifi();     //init wifi connection
  setup_mcp2515();  //init MCP2515 CAN
  
  client.setServer(mqtt_server, 1883); 
  client.setCallback(callback);
}


void publish_string(char* sensor_msg)
{
  char msg_buffer[50];
  
  if (!client.connected()) {
    reconnect();
  }  
  
  // MQTT client loop processing
  client.loop();
  
  sprintf(msg_buffer, "{\"value\": \"%s\"}", sensor_msg); //format for MQTT (JSON)
  
  Serial.println(msg_buffer); //print msg to serial

  client.publish(stringTopic, msg_buffer); //publish to DeviceHub.net (MQTT)  
}

void publish_sensor(double sensor_val, char* sensor_name)
{
  char sensorTopic[256];    //= "/a/"API_KEY"/p/"PROJECT_ID"/d/"DEVICE_UUID"/sensor/"SENSOR_NAME"/data";
  char msg_buffer[50];
  char sensor_val_str[50];

  //prepare topic for MQTT
  sprintf(sensorTopic, "/a/"API_KEY"/p/"PROJECT_ID"/d/"DEVICE_UUID"/sensor/%s/data", sensor_name);
     
  if (!client.connected()) {
    reconnect();
  }  

  // MQTT client loop processing
  client.loop();

  dtostrf(sensor_val, 4,1, sensor_val_str);

  sprintf(msg_buffer, "{\"value\": \"%s\"}", sensor_val_str); //format for MQTT (JSON)
  
  Serial.println(msg_buffer); //print msg to serial

  client.publish(sensorTopic, msg_buffer); //publish to DeviceHub.net (MQTT)  
}

void CAN_receive_all()
{
   char sensor_msg[50];
   
   CAN0.readMsgBuf(&rxId, &len, rxBuf);    // Read data: len = data length, buf = data byte(s)
  
    if((rxId & 0x80000000) == 0x80000000){  // Determine if ID is standard (11 bits) or extended (29 bits)
      sprintf(msgString, "Extended ID: 0x%X  DLC: %d  Data:", (rxId & 0x1FFFFFFF), len);
    }
    else {
      sprintf(msgString, "Standard ID: 0x%.3lX       Length: %1d  Data:", rxId, len);
    }
    
    Serial.print(msgString);
        
    if((rxId & 0x40000000) == 0x40000000){    // Determine if message is a remote request frame.
      sprintf(msgString, " REMOTE REQUEST FRAME");
      Serial.print(msgString);
    } else {
      for(byte i = 0; i<len; i++){
        sprintf(sensor_msg,"0x%X", rxBuf[i]);
        Serial.print(sensor_msg);
        publish_string(sensor_msg);
      }
    }
}

void CAN_receive_RPM()
{
  int msg_PGN = 0;
  int raw_data = 0;
  double sensor_data;
  
  char sensor_msg[64];
  CAN0.readMsgBuf(&rxId, &len, rxBuf);    // Read data: len = data length, buf = data byte(s)

  msg_PGN = (rxId & 0xFFFFFF)>>8;
  
  //Serial.printf("PGN: %d",msg_PGN);
  //Serial.println();

  //J1939 ENGINE SPEED
  if(msg_PGN == 61444) {
    raw_data = (rxBuf[4] << 8) | rxBuf[3];       // Combine 2 bytes
    sensor_data = raw_data*0.125;
    dtostrf(sensor_data, 4,1,sensor_msg);
      
    //Serial.printf("%s RPM",sensor_msg);
    //Serial.println();
    publish_sensor(sensor_data, SENSOR_RPM);
  }    

  //J1939 ENGINE TEMP
  if(msg_PGN == 65262) {
    raw_data = rxBuf[0];       // Combine 2 bytes
    sensor_data = (raw_data-40);
    dtostrf(sensor_data, 4,1,sensor_msg);
      
    Serial.printf("%s C",sensor_msg);
    Serial.println();
    
    publish_sensor(sensor_data, SENSOR_TEMP);
  }    
}


void loop()
{
  /********************************************************************/
  /* On receipt of CAN Message, publish message data to DeviceHub.net */
  /********************************************************************/
      
  /* RECEIVE MESSAGE */
  if(!digitalRead(CAN0_INT))                // If CAN0_INT pin is low, read receive buffer
  {
      //CAN_receive_all();
      CAN_receive_RPM();
  }  
}

/*********************************************************************************************************
  END FILE
*********************************************************************************************************/