/*
 * Project Electron-WorkingProto
 * Description:
 * Author:
 * Date:
 */
#define MCP2561_STBY D6
#define CAN_BAUD_RATE 250000
#define SERIAL_BAUD_RATE 19200

#define PGN_BIT_MASK 0x3FFFF00  //masks 29 bit ID to PGN
#define PGN_EEC1  61444<<8  //engine speed PGN
#define PGN_ET1   65262<<8  //coolant temp PGN
#define PGN_EFL   65263<<8  //oil pressure PGN

#define TRANSMIT_RATE 1000 //time between updates in ms

CANChannel can(CAN_C4_C5); //initialise the CAN Channel utilising C4 and C5

//DEFINE FUNCTIONS
int engineAction(String command); //particle function to start/stop engine
int validateCAN() {
  if(can.errorStatus() == CAN_ERROR_PASSIVE) {
    Serial.println("CAN ERROR: Passive");
    return -1;
  } else if(can.errorStatus() == CAN_BUS_OFF) {
    Serial.println("CAN ERROR: Off");
    return -1;
  } else if(can.isEnabled()) {
    return 1;
  } else {
    return -1;
  }
}

//GLOBAL VARIABLES FOR ENGINE DATA
int J1939_EngineSpeed = 0;
int J1939_OilPressure = 0;
int J1939_CoolantTemp = 0;
unsigned long lastTime = 0;

void setup() {
  //pinMode(MCP2561_STBY, OUTPUT); //Configure MCP2561 Standby output

  Serial.begin(SERIAL_BAUD_RATE); //Initialise serial console
  can.begin(CAN_BAUD_RATE);  //Initialise CAN Channel
  validateCAN();

  //Configure message filters
  can.addFilter(PGN_EEC1, PGN_BIT_MASK, CAN_FILTER_EXTENDED); //filter id for engine speed PGN
  can.addFilter(PGN_ET1,  PGN_BIT_MASK, CAN_FILTER_EXTENDED); //filter id for coolant temm PGN
  can.addFilter(PGN_EFL,  PGN_BIT_MASK, CAN_FILTER_EXTENDED); //filter id for oil pressure PGN
}

int sendMessage() {
  CANMessage message_tx;
  message_tx.id = 0x101;
  message_tx.len = 1;
  message_tx.data[0] = 42;

  if(can.transmit(message_tx)) { //send a CAN message_rx
    Serial.println("tx queued");
  }
}

int parseMessage(CANMessage message_rx) {

  if((message_rx.id & PGN_BIT_MASK) == PGN_EEC1) {
    J1939_EngineSpeed = (((message_rx.data[4]<<8)+message_rx.data[3])*0.125); //2 bytes long, 0.125 scaling, no offset
  }
  else if((message_rx.id & PGN_BIT_MASK) == PGN_ET1) {
    J1939_CoolantTemp =  (message_rx.data[0]-40);
  }
  else if((message_rx.id & PGN_BIT_MASK) == PGN_EFL) {
    J1939_OilPressure =  (message_rx.data[3]*4);
  }
}

void loop() {
  unsigned long now = millis();
  //digitalWrite(MCP2561_STBY,LOW); //Output LOW to bring MCP2561 out of STBY mode
  validateCAN();

  CANMessage message_rx;
  if(can.receive(message_rx)) {
    parseMessage(message_rx);
  }

  if ((now - lastTime) >= TRANSMIT_RATE) {
    lastTime = now;
    Serial.print(J1939_EngineSpeed);
    Serial.println("RPM");

    Serial.print(J1939_CoolantTemp);
    Serial.println("C");

    Serial.print(J1939_OilPressure);
    Serial.println("kPA");

    Serial.println();
  }
}
