/*
 * Project Electron_J1939Telemetry
 * Description: Firmware for Particle Electron to support J1939 messages and transmit via 3G
 * Author: Alex Barry
 * Date: 15/07/2017
 */
#define MCP2561_STBY D6
#define CAN_BAUD_RATE 250000
#define SERIAL_BAUD_RATE 19200

#define PGN_BIT_MASK 0x3FFFF00  //masks 29 bit ID to PGN
#define PGN_EEC1  61444<<8  //engine speed PGN
#define PGN_ET1   65262<<8  //coolant temp PGN
#define PGN_EFL   65263<<8  //oil pressure PGN
#define PGN_DM1   65226<<8  //Diagnostic message PGN

#define PGN_STRTRQST   65340<<8 //Proprietary PGN used for StartRQST Message
#define CAN_SRC_ADDR   60 //source address for Particle transmit messages

#define SERIAL_TX_RATE 1000 //time between serial updates in ms
#define CAN_TX_RATE 1000 //time between CAN message transmits in ms

CANChannel can(CAN_C4_C5); //initialise the CAN Channel utilising C4 and C5

//GLOBAL VARIABLES FOR ENGINE DATA
int EngineSpeed = 0;
int OilPressure = 0;
int CoolantTemp = 0;

int J1939_DM1_SPN = 0;
int J1939_DM1_FMI = 0;

int EngineStartRQST = 1;

unsigned long lastSerialTx = 0;
unsigned long lastCANTx = 0;


int serialUpdate() {
  Serial.print(EngineSpeed);
  Serial.println(" RPM");

  Serial.print(CoolantTemp);
  Serial.println(" C");

  Serial.print(OilPressure);
  Serial.println(" kPA");

  if(EngineStartRQST){
    Serial.println("<<Requesting Engine Start>>");
  }

  Serial.println();
}

int sendStartRQST() {
  CANMessage message_tx;
  message_tx.extended = TRUE;
  message_tx.id = PGN_STRTRQST | CAN_SRC_ADDR;
  message_tx.len = 1;
  message_tx.data[0] = 1;

  if(!can.transmit(message_tx)) { //send a CAN message_rx
    Serial.println("Failed to Queue CAN Tx");
  }
}

void setup() {
  //Initialise serial console
  Serial.begin(SERIAL_BAUD_RATE);

  //Initialise CAN Channel
  can.begin(CAN_BAUD_RATE);

  //Configure message filters
  can.addFilter(PGN_EEC1, PGN_BIT_MASK, CAN_FILTER_EXTENDED); //filter id for engine speed PGN
  can.addFilter(PGN_ET1,  PGN_BIT_MASK, CAN_FILTER_EXTENDED); //filter id for coolant temm PGN
  can.addFilter(PGN_EFL,  PGN_BIT_MASK, CAN_FILTER_EXTENDED); //filter id for oil pressure PGN
  can.addFilter(PGN_DM1,  PGN_BIT_MASK, CAN_FILTER_EXTENDED); //filter id for diagnostic messages

  //Configure Particle Variables (Available for pull requests)
  Particle.variable("EngSpeed", EngineSpeed);
  Particle.variable("CoolTemp", CoolantTemp);
  Particle.variable("OilPres", OilPressure);
}

bool comparePGN(int id,int PGN) {
  return ((id & PGN_BIT_MASK) == PGN);
}

int parseMessage(CANMessage message_rx) {

  if(comparePGN(message_rx.id,PGN_DM1)) {
    char publishString[8]; //string buffer for publish function

    //Pull SPN and FMI from DTC packet
    J1939_DM1_SPN = message_rx.data[2];
    J1939_DM1_FMI = message_rx.data[4];

    //Print big warning message to serial
    Serial.println("!!!!!!!!!ALARM!!!!!!!!!!");
    Serial.print("SPN: ");
    Serial.println(J1939_DM1_SPN);
    Serial.print("FMI: ");
    Serial.println(J1939_DM1_FMI);
    Serial.println("!!!!!!!!!!!!!!!!!!!!!!!!");
    Serial.println();

    //Format SPN And FMI into strings and publish to Particle
    sprintf(publishString, "%d", J1939_DM1_SPN);
    Particle.publish("S", publishString); //publish SPN
    sprintf(publishString, "%d", J1939_DM1_FMI);
    Particle.publish("F", publishString); //publish FMI
  }
  else if(comparePGN(message_rx.id, PGN_EEC1)) {
    EngineSpeed = (((message_rx.data[4]<<8)+message_rx.data[3])*0.125); //2 bytes long, 0.125x scaling, 0 offset
  }
  else if(comparePGN(message_rx.id, PGN_ET1)) { //1 byte long, 1x scaling, -40 deg offset
    CoolantTemp =  (message_rx.data[0]-40);
  }
  else if(comparePGN(message_rx.id, PGN_EFL)) { //1 byte long, 4x scaling, 0 offset
    OilPressure =  (message_rx.data[3]*4);
  }
}

void loop() {
  unsigned long now = millis(); //used for sending periodic message

  //Instantiate CANMessage and parse one that has been accepted by filter
  CANMessage message_rx;
  if(can.receive(message_rx)) {
    parseMessage(message_rx);
  }

  //Send message to serial  periodically
  if ((now - lastSerialTx) >= SERIAL_TX_RATE) {
    lastSerialTx = now;
    serialUpdate();
  }

  //Transmit engine run CAN message periodically
  if (EngineStartRQST & (now - lastCANTx) >= SERIAL_TX_RATE) {
    lastCANTx = now;
    sendStartRQST();
  }
}
